# Lab 6: Testing YATL

This week, we are going to test the YATL (Yet Another Todo List) application.

## Exercise 1: Setting up the Project

### Exercise 1.1: Cloning the Repository

1. Clone the repository from GitHub using the following command:

   ```bash
     git clone -b exercise-2-5-solution https://gitlab.surrey.ac.uk/com_1028_labs/lab-5.git yatl
   ```

The above command will clone the repository into a directory called `yatl`.

2. Import the project folder into your Eclipse Workspace: `File -> Open Projects from File System -> Directory -> yatl`

### Exercise 1.2: Installing the Dependencies

In order to test the YATL application, we are going to need to install three, amazing, testing libraries:

- junit - JUnit for unit testing
- mockito - Mockito for mocking objects in unit tests
- rest-assured - Testing and validating our APIs

Recall, we can find Maven dependencies by searching the [Maven Repository](https://mvnrepository.com/). Once we have found the dependencies, we can locate the XML code to the dependencies section of the `pom.xml` file.

1. See if you can find the above dependencies on the Maven Repository. When you have found them, add them to the `pom.xml` file. You should install the latests versions of the dependencies, at the time of writing:

   - junit: 5.10.2
   - mockito: 5.11.0
   - rest-assured: 5.4.0

[Check your POM.xml file against the solutions]()

### Exercise 1.3: Setting up the Test Directory
