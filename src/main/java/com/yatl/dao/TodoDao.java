package com.yatl.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.yatl.model.Todo;
import com.yatl.util.Database;

public class TodoDao {

	private final Connection conn;

	public TodoDao() {
		Database database = Database.getInstance();
		conn = database.getConnection();
	}

	public List<Todo> getAll() throws SQLException {

		List<Todo> todos = new ArrayList<>();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM todos");
		while (rs.next()) {

			todos.add(new Todo(rs.getInt("id"), rs.getString("title"), rs.getBoolean("completed")));

		}

		return todos;

	}

	public List<Todo> getByStatus(Boolean status) throws SQLException {

		List<Todo> todos = new ArrayList<>();
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM todos WHERE completed = " + status);
		while (rs.next()) {

			todos.add(new Todo(rs.getInt("id"), rs.getString("title"), rs.getBoolean("completed")));

		}

		return todos;

	}

	public Todo getById(int id) throws SQLException {

		String statement = "SELECT * FROM todos WHERE id = ?";
		PreparedStatement stmt = conn.prepareStatement(statement);
		stmt.setInt(1, id);
		ResultSet rs = stmt.executeQuery();

		if (rs.next()) {
			return new Todo(rs.getInt("id"), rs.getString("title"), rs.getBoolean("completed"));
		}

		return null;

	}

}
