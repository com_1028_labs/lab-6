package com.yatl;

import com.yatl.controller.TodoController;
import com.yatl.dao.TodoDao;

import io.javalin.Javalin;

public class AppConfig {

	public static Javalin startServer(int port) {

		Javalin app = Javalin.create().start(port);
		TodoDao todoDao = new TodoDao();
		TodoController todoController = new TodoController(todoDao);

		app.get("/todos", todoController::getAllTodos);
		app.get("/todos/{id}", todoController::getTodoById);

		return app;

	}

}
