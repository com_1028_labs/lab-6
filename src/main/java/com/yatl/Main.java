package com.yatl;

import com.yatl.util.Database;

public class Main {

	public static void main(String[] args) {

		String databasePath = "src/main/resources/database.db";
		String url = "jdbc:sqlite:" + databasePath;
		Database.getInstance(url);
		AppConfig.startServer(8000);

	}

}
