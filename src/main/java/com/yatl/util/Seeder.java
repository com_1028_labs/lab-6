package com.yatl.util;


import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Seeder {
	URL url = Seeder.class.getClassLoader().getResource("");

	public Seeder() {
		try {
			String databasePath = "src/main/resources/database.db";
			String url = "jdbc:sqlite:" + databasePath;
			System.out.println(url);
			Connection conn = DriverManager.getConnection(url);

			if (conn != null) {
				System.out.println("Connected to the database");
				Statement stmt = conn.createStatement();
				stmt.execute("DROP TABLE IF EXISTS todos");
				stmt.execute("CREATE TABLE todos (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT NOT NULL, completed BOOLEAN DEFAULT 0)");
				stmt.execute("INSERT INTO todos (title) VALUES ('Learn Java')");
				stmt.execute("INSERT INTO todos (title) VALUES ('Vote for Joe')");
				stmt.execute("INSERT INTO todos (title) VALUES ('Buy a car')");
				stmt.execute("INSERT INTO todos (title, completed) VALUES ('Ace Joes Module', 1)");
	

				// get the data
				ResultSet rs = stmt.executeQuery("SELECT * FROM todos");
				while (rs.next()) {
					System.out.println(rs.getInt("id") + " " + rs.getString("title") + " "
							+ rs.getBoolean("completed"));
				}

				conn.close();
			} else {
				System.out.println("Failed to connect to the database");
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}