package com.yatl.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {

	private static Database instance; 
	private Connection connection;
	
	private Database(String path) {
		try {
			this.connection = DriverManager.getConnection(path);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	public static Database getInstance(String path) {
		
		if (instance == null) {
			instance = new Database(path);
		}
		
		return instance;
		
	}
	
	public static Database getInstance() {

		if (instance == null) {
			throw new IllegalStateException("Database instance not set");
		}
		return instance;
}
	
	public Connection getConnection() {
		return connection;
	}
}
