package com.yatl.controller;

import java.sql.SQLException;
import java.util.List;

import com.yatl.dao.TodoDao;
import com.yatl.model.Todo;

import io.javalin.http.Context;

public class TodoController {

	TodoDao todoDao;

	public TodoController(TodoDao todoDao) {
		this.todoDao = todoDao;
	}

	public void getAllTodos(Context ctx) {
		try {
			List<Todo> todos = todoDao.getAll();
			String status = ctx.queryParam("status");
			if (status != null) {

				if (status.equals("completed")) {
					todos = todoDao.getByStatus(true);
				} else if (status.equals("active")) {
					todos = todoDao.getByStatus(false);
				}
			} else {
				todos = todoDao.getAll();
			}
			ctx.json(todos);
		} catch (SQLException e) {
			ctx.status(500);
			ctx.result("Internal server error");
		}
	}

	public void getTodoById(Context ctx) {

		try {
			int id = Integer.parseInt(ctx.pathParam("id"));
			Todo todo = todoDao.getById(id);
			if (todo != null) {
				ctx.json(todo);
			} else {
				ctx.status(404);
				ctx.result("Todo not found");
			}
		} catch (SQLException e) {
			ctx.status(500);
			ctx.result("Internal server error");
		}
	}

}
